[<RequireQualifiedAccess>]
module Gallery

open Elmish
open Elmish.React
open Feliz
open Fable.Core
open Fable.React
open Fable.React.Props
open Browser
open Browser.Types
open Thoth.Fetch
open Thoth.Json

/////////////////////////////////////////////////////////////////////////////////////////
// Constant values used in the application
/////////////////////////////////////////////////////////////////////////////////////////

// This is the site we retrieve the photos from.  All photo urls are appended to this
// base.
let private canvasName = "main-canvas"

let private random = System.Random()

/////////////////////////////////////////////////////////////////////////////////////////
// Base types used in the application
/////////////////////////////////////////////////////////////////////////////////////////

type Photo = 
    { Url : string // The URL of the photo relative to 'urlPrefix' 
      Size : int
      Title : string }

    static member Decoder : Decoder<Photo> =
        Decode.object (fun get -> {
            Url   = get.Required.Field "url"   Decode.string
            Size  = get.Required.Field "size"  Decode.int
            Title = get.Optional.Field "title" Decode.string
                    |> Option.defaultValue "(untitled)"
        })


type SlideEvtDetail =
    abstract userSlidTo: int
    abstract sliderName: string


type SlideEvt =
   inherit Event
   abstract detail: SlideEvtDetail


type SelectedThumbnail = {
    Url : string
}


type ThumbnailSize =
    | Small
    | Medium
    | Large


type Status =
    | Loading
    | Loaded of (Photo list) * string
    | Errored of string


type FilterSetting = {
    Name : string
    Amount : int
}


type FilterOptions = {
    Url : string
    Filters : FilterSetting list
}


type FilterLib =
    abstract apply: string * FilterOptions -> unit
    abstract addActivityListener: (string -> unit) -> unit  // Get callbacks from JS
    abstract pastaVersion: float


// Used by App.fs
[<ImportAll("../Client/public/js/pasta.js")>]
let filterLib : FilterLib = jsNative


// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
    | GotActivity of string
    | InitialRender
    | PhotoClicked of SelectedThumbnail
    | GotRandomPhoto of Photo
    | SurpriseMeClicked
    | SizeClicked of ThumbnailSize
    | GotPhotos of Result<Photo list, FetchError>
    | SlidHue of int
    | SlidRipple of int
    | SlidNoise of int


// The application's Model type.
// The model holds data that you want to keep track of while the application is running.
type Model = { 
    Title : string
    Activity : string
    InitialRender : bool
    Status : Status
    ChosenSize : ThumbnailSize
    Hue : int
    Ripple : int
    Noise : int
}


let private title = "PhotoGroove Gallery"


let initialModel = { 
    Title = title
    Activity = ""
    InitialRender = false
    Status = Loading
    ChosenSize = Medium
    Hue = 0
    Ripple = 0
    Noise = 0
}


/////////////////////////////////////////////////////////////////////////////////////////
// Support functions used in the application
/////////////////////////////////////////////////////////////////////////////////////////

let private applyFilters (model : Model) (url : string option) =
    let filters : FilterSetting list = [
        { Amount = model.Hue;    Name = "Hue" }
        { Amount = model.Ripple; Name = "Ripple" }
        { Amount = model.Noise;  Name = "Noise" }
    ]
    let filtOpts : FilterOptions =
        match url, model.Status with
        | Some(u), _ ->
            let url = Shared.urlPrefix + "large/" + u
            { Url = url; Filters = filters }
        | None, Loaded (_,u) -> 
            let url = Shared.urlPrefix + "large/" + u
            { Url = url; Filters = filters }
        | _,_ -> 
            { Url = ""; Filters = filters }
    filterLib.apply (canvasName, filtOpts)


let private onSlideMsg (toMsg: int -> Msg) =
    Decode.at ["detail"; "userSlidTo"] Decode.int
    |> Decode.map toMsg 
    

/// Function to create a range-slider custom element
let private rangeSlider (max: int) (magnitude: int) (name: string) =
    // Note:  in Elm these parameters are not tupled.
    // Also, apparently Elmish uses `domEl` instead of `node` to create dom elements
    // Now that we have moved to Feliz, is there a better way to do this?
    domEl "range-slider" [
                HTMLAttr.Custom ("max", max)
                HTMLAttr.Custom ("val", magnitude)
                HTMLAttr.Custom ("name", name)

    ] []


/// Turn the response of `loadPhotos` into a Msg depending on the status of the response
let private photoResposeToMsg (photoRes : Result<Photo list, FetchError>) =
    GotPhotos photoRes


/// Retrieve the list of photos from the website
let private loadPhotos () =
    promise {
        let url = Shared.urlPrefix + "list.json"
        return! Fetch.tryGet (url, decoder = (Decode.list Photo.Decoder))
    }


// Take the response (Ok or Error) and modify the model appropriately (Status <- Loaded, or
// Status <- Errored)
let private processPhotoResult model (res: Result<Photo list, FetchError>) =
    match res with
    | Ok lst -> 
        match lst with 
        | h::t -> 
            applyFilters model None
            { model with Status = Loaded (lst, h.Url) }, Cmd.none
        | [] -> 
            applyFilters model None
            { model with Status = Loaded (lst, "") }, Cmd.none
    | Error m -> 
        let s = match m with
                   | PreparingRequestFailed e -> "Failed to prepare Request: " + e.ToString()
                   | DecodingFailed s -> s
                   | FetchFailed r -> "Failed to fetch Photos: " + r.ToString()
                   | NetworkError e -> "Network error fetching Photos"
        { model with Status = (Errored s) }, Cmd.none


/// Generate a random integer between low and high, inclusive
let private randInt (low : int) (high : int) =
    random.Next(low, high)


/// Return a randomly selected element from a list.  Assumes the list is non-empty
let private uniform (firstElement: 'a) (rest: 'a list) : 'a =
    let maxIndex = 1 + (rest |> List.length)
    let index = randInt 0 maxIndex
    if index = 0 then firstElement
    else rest |> List.skip (index-1) |> List.head


/// Return a new status with a potentially modified selected URL.
let private selectUrl (url: string) (status: Status) =
    match status with
    | Loaded (photos, _) -> Loaded (photos, url)
    | Loading -> status
    | Errored _ -> status


/// Convert a ThumnailSize value to a class name
let private sizeToClass (size : ThumbnailSize) =
    match size with
    | Small -> "small"
    | Medium -> "medium"
    | Large -> "large"


/// Convert a ThumbnailSize value to a label string (capitalized)
let private sizeToLabel (size : ThumbnailSize) =
    match size with
    | Small -> "Small"
    | Medium -> "Medium"
    | Large -> "Large"


//////////////////////////////////////////////////////////////////////////////
/// Create view elements
//////////////////////////////////////////////////////////////////////////////

/// Create a filter slider element
let private viewFilter (name: string) (magnitude: int) = 
    Html.div [
        prop.className "filter-slider"
        prop.children [
            Html.label [
                prop.text name
            ]
            (rangeSlider 11 magnitude name)
            Html.text (sprintf "%d" magnitude)
        ]
    ]


/// Create a radiobutton size chooser element for a given ThumbnailSize value
let private viewSizeChooser (dispatch : Msg -> unit) (size : ThumbnailSize) =
    Html.label [
         prop.children [
           Html.text (sizeToLabel size)
           Html.input [
                prop.type' "radio"
                prop.name "size"
                prop.onClick (fun _ -> dispatch (SizeClicked size) )
            ]
        ]
    ]


/// Create a single thumbnail for the gallery
let private viewThumbnail (dispatch : Msg -> unit) (selectedUrl : string) (thumb : Photo) =
    let classes = 
        match selectedUrl with
        | s when s = thumb.Url -> ["selected"]
        | _ -> []
    Html.img [
        prop.src ( Shared.urlPrefix + thumb.Url)
        prop.title (thumb.Title + " [" + (sprintf "%d" thumb.Size) + " KB]")
        prop.classes classes
        prop.onClick (fun _ -> dispatch (PhotoClicked { Url = thumb.Url } ) )
    ]


/// Create the page elements when we have loaded photos to view
let private viewLoaded (dispatch : Msg -> unit) (photos : Photo list) (selectedUrl : string) (model : Model) =
    let createThumbnail = viewThumbnail dispatch selectedUrl
    let createSizeChooser = viewSizeChooser dispatch
    let sizeClass = sizeToClass model.ChosenSize

    if (not model.InitialRender) then
        // Send an InitialRender message so that we can apply the filters to the loaded photos.
        dispatch InitialRender
    else ()

    [
        Html.h1 [
            prop.text "PhotoGroove"
        ]
        Html.button [
            prop.onClick (fun _ -> dispatch SurpriseMeClicked)
            prop.text "Surprise Me!"
        ]
        Html.div [
            prop.className "activity"
            prop.text model.Activity
        ]
        Html.div [
            prop.className "filters"
            prop.children [
                viewFilter "Hue" model.Hue
                viewFilter "Ripple" model.Ripple
                viewFilter "Noise" model.Noise
            ]
        ]
        Html.h3 [ prop.text "Thumbnail Size:" ]
        Html.div [
            prop.id "choose-size"
            prop.children ([ Small; Medium; Large] |> List.map createSizeChooser)
        ]
        Html.div [
            prop.id "thumbnails"
            prop.className sizeClass
            prop.children ( photos |> List.map createThumbnail)
        ]
        Html.canvas [
            prop.className "large"
            prop.id canvasName
        ]
    ]


let private viewLoading (dispatch : Msg -> unit) (model : Model) =
    [ Html.h1 [
          prop.text "Loading Photos"
      ]
    ]


/////////////////////////////////////////////////////////////////////////////////////////
// Primary interface functions used in the application
/////////////////////////////////////////////////////////////////////////////////////////

// The view function creates the page's visual structure.
let view (model : Model) (dispatch : Msg -> unit) =
    div [ Class "content"] <|
        match model.Status with
        | Loaded (photos, selected) ->
            viewLoaded dispatch photos selected model
        | Loading -> viewLoading dispatch model
        | Errored msg -> [ str ("Error: " + msg) ]


// The update function computes the next state of the application based on the current 
// state and the incoming events/messages.
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (model : Model) : Model * Cmd<Msg> =
    match msg with
    | GotActivity act ->
        { model with Activity = act }, Cmd.none
    | InitialRender ->
        applyFilters model None
        {model with InitialRender = true }, Cmd.none
    | PhotoClicked sel -> 
        applyFilters model (Some sel.Url)
        { model with Status = selectUrl sel.Url model.Status}, Cmd.none
    | GotRandomPhoto p -> 
        applyFilters model (Some p.Url)
        { model with Status = selectUrl p.Url model.Status }, Cmd.none
    | SurpriseMeClicked -> 
        match model.Status with
        | Loaded (h::t, _) -> 
            uniform h t           // Select a random photo from the list
            |> GotRandomPhoto     // Convert it to a Msg type
            |> Cmd.ofMsg          // Wrap the Msg in a Cmd
            |> fun c -> model, c  // Return the tupled response
        | Loaded _ -> model, Cmd.none
        | Loading -> model, Cmd.none
        | Errored _ -> model, Cmd.none
    | SizeClicked size -> { model with ChosenSize = size }, Cmd.none
    | GotPhotos res -> processPhotoResult model res
    | SlidHue(h) -> 
        applyFilters model None
        { model with Hue = h }, Cmd.none
    | SlidRipple(r) -> 
        applyFilters model None
        { model with Ripple = r }, Cmd.none
    | SlidNoise(n) -> 
        applyFilters model None
        { model with Noise = n },Cmd.none


/////////////////////////////////////////////////////////////////////////////////////////
// Application Bootstrap
/////////////////////////////////////////////////////////////////////////////////////////

let activitySub (_: Model) =
    let hdlr (dispatch: Msg -> unit) (s: string) =
        dispatch (GotActivity s)
    let sub dispatch = filterLib.addActivityListener (hdlr dispatch)
    Cmd.ofSub sub


let sliderSub (_: Model) =
    let hdlr dispatch evt =
        let slideEvt = evt |> unbox<SlideEvt>
        match slideEvt.detail.sliderName with
        | "Hue" -> dispatch (SlidHue slideEvt.detail.userSlidTo)
        | "Ripple" -> dispatch (SlidRipple slideEvt.detail.userSlidTo)
        | "Noise" -> dispatch (SlidNoise slideEvt.detail.userSlidTo)
        | _ -> ()
    let sub dispatch =
        window.addEventListener ("slide", (hdlr dispatch))
    Cmd.ofSub sub


// Defines the initial state and initial command (= side-effect) of the application
let init (flags: float) : Model * Cmd<Msg> =
    let cmd = Cmd.OfPromise.perform loadPhotos () photoResposeToMsg
    {initialModel with Activity = (sprintf "Initializing Pasta v" + flags.ToString())}, cmd


