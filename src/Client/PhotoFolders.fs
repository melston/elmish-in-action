[<RequireQualifiedAccess>]
module PhotoFolders

open Elmish
open Elmish.React
open Feliz
open Fable.Core
open Fable.React
open Fable.React.Props
open Browser
open Browser.Types
open Thoth.Fetch
open Thoth.Json

type Photo = {
    Title : string
    Size : int
    RelatedUrls : string list
    Url : string
}


type Folder = {
    Name : string
    PhotoUrls : string list
    Expanded : bool
    SubFolders : Folder list
}


type Model = {
    Title : string
    SelectedPhotoUrl : string option
    Photos : Map<string, Photo>
    Root : Folder
}


type JsonPhoto = {
    Title : string
    Size : int
    RelatedPhotos : string list  // Renamed to match the serialized json (in snake_case)
}


// From the Shared.urlPrefix/folders/list
type JsonFolder = {
    Name : string
    Photos : Map<string, JsonPhoto>
    Subfolders : JsonFolder list
}


let private title = "PhotoGroove Folders"


let private jsonFolderDecoder =
    Decode.Auto.generateDecoder<JsonFolder>(caseStrategy = SnakeCase)


let rec private getPhotosFromJsonFolder (js : JsonFolder) : Map<string, JsonPhoto> =
    let join (a: Map<'a, 'b>) (b: Map<'a, 'b>) =
        Map ( Seq.concat [(Map.toSeq a); (Map.toSeq b)] )
    (js.Subfolders |> List.map getPhotosFromJsonFolder) 
    |> List.fold join Map.empty
    |> join js.Photos


let rec private getFoldersFromJsonFolder (js : JsonFolder) : Folder =
    { Name = js.Name
      Expanded = true
      PhotoUrls = js.Photos |> Map.toList |> List.map fst
      SubFolders = js.Subfolders |> List.map getFoldersFromJsonFolder
    }


let private jsonFolderToModel (js : JsonFolder) : Model =
    let photos = getPhotosFromJsonFolder js
                 |> Map.map (fun k v -> 
                   {  Title = v.Title
                      Size = v.Size
                      RelatedUrls = v.RelatedPhotos
                      Url = k
                    })

    let folder = getFoldersFromJsonFolder js
    {  Title = title
       SelectedPhotoUrl = None
       Photos = photos
       Root = folder
    }


type FolderPath =
    | End
    | Subfolder of (int * FolderPath)


type Msg =
    | ClickedPhoto of string
    | GotInitialModel of Result<Model, FetchError> // was Http.Error in EIA
    | ClickedFolder of FolderPath


let initialModel = {
    Title = title 
    SelectedPhotoUrl = None 
    Photos = Map.empty
    Root = {
        Name = "Loading..."
        PhotoUrls = []
        Expanded = false
        SubFolders = []
    }
}

let private modelDecoder : Decoder<Model> =
    jsonFolderDecoder |> Decode.map jsonFolderToModel


/// Turn the response of `loadFolders` into a Msg depending on the status of the response
let private folderResposeToMsg (folderRes : Result<Model, FetchError>) =
    printfn "InitialModel : %A" folderRes
    GotInitialModel folderRes


let rec private toggleExpanded path (folder : Folder) =
    match path with
    | End -> { folder with Expanded = not folder.Expanded }
    | Subfolder (targetIdx, remainingPath) ->
        let transform (idx: int) (currSubFolder: Folder) =
            if idx = targetIdx then toggleExpanded remainingPath currSubFolder
            else currSubFolder
        let subfolders = folder.SubFolders |> List.mapi transform
        { folder with SubFolders = subfolders }


/// Retrieve the list of photos from the website
let private loadFolders () : JS.Promise<Result<Model, FetchError>> =
    promise {
        printfn "Loading folders"
        let url = Shared.urlPrefix + "folders/list"
        return! Fetch.tryGet (url, decoder = modelDecoder)
    }


let rec private appendIndex (index: int) (path: FolderPath) =
    match path with
    | End ->                        Subfolder (index, End)
    | Subfolder (sfIdx, remPath) -> Subfolder (sfIdx, (appendIndex index remPath))


let private viewPhoto (dispatch : Msg -> unit) (url: string) =
    Html.div [
        prop.className "photo"
        prop.onClick (fun _ -> dispatch (ClickedPhoto url))
        prop.text url
    ]


let rec private viewFolder (dispatch : Msg -> unit) (path : FolderPath) (folder : Folder) =
    let viewSubFolders (idx: int) (subFolder: Folder) =
        viewFolder dispatch (appendIndex idx path) subFolder

    let folderLabel = Html.label [
        prop.text folder.Name
        prop.onClick (fun _ -> dispatch (ClickedFolder path))
    ]

    let contents =
        List.append (folder.SubFolders |> List.mapi viewSubFolders)
                    (folder.PhotoUrls |> List.map (viewPhoto dispatch)) 

    if (folder.Expanded) then
        Html.div [
            prop.className "folder expanded"
            prop.children [
                folderLabel
                Html.div [
                    prop.className "subfolders"
                    prop.children contents
                ]
            ]
        ]
    else 
        Html.div [
            prop.className "folder"
            prop.children [ folderLabel ]
        ]


// Instead of doing this inline with the "content" div like the book has it we will do this
// as a separate function.  It makes the resulting code a bit cleaner in the Elmish/Feliz
// environment.
let private viewFolders (dispatch : Msg -> unit) (root : Folder) =
    Html.div [
        prop.className "folders"
        prop.children [
            Html.h1 [ prop.text "Folders"]
            viewFolder dispatch End root 
        ]
    ]


let private viewRelatedPhotos (dispatch : Msg -> unit) (url : string) =
    Html.img [
        prop.className "related-photo"
        prop.onClick (fun _ -> dispatch (ClickedPhoto url ) )
        prop.src (Shared.urlPrefix + "photos/" + url + "/thumb")
    ]


let private viewSelectedPhoto (dispatch : Msg -> unit) (photo : Photo) =
    Html.div [
        prop.className "selected-photo"
        prop.children [
            Html.h2 [ prop.text photo.Title]
            Html.img [
                prop.src (Shared.urlPrefix + "photos/" + photo.Url + "/full")
            ]
            Html.span [ prop.text (sprintf "%d KB" photo.Size)]
            Html.h3 [ prop.text "Related"]
            Html.div [
                prop.className "related-photos"
                prop.children (photo.RelatedUrls |> List.map (viewRelatedPhotos dispatch)) 
            ]
        ]
    ]


let view (model : Model) (dispatch : Msg -> unit) =
    let photoOpt = 
        model.SelectedPhotoUrl
        |> Option.bind (fun u -> Map.tryFind u model.Photos)
    let photoElement = 
        match photoOpt with
            | Some photo -> viewSelectedPhoto dispatch photo
            | None -> Html.p [ prop.text "No photo available or selected"]
    Html.div [
        prop.className "content"
        prop.children [
            (viewFolders dispatch model.Root)
            photoElement
        ]
    ]


let update (msg : Msg) (model : Model) : ( Model * Cmd<Msg> ) =
    match msg with
    | ClickedPhoto url -> { model with SelectedPhotoUrl = Some url }, Cmd.none
    | GotInitialModel r ->
        match r with
        | Ok m -> 
            printfn "Got initial response"
            m, Cmd.none
        | Error e -> 
            printfn "Got error response"
            model, Cmd.none
    | ClickedFolder path ->
        { model with Root = (toggleExpanded path model.Root) }, Cmd.none


let init () :  ( Model * Cmd<Msg>) =
    let cmd = Cmd.OfPromise.perform loadFolders () folderResposeToMsg
    initialModel, cmd


