module App

open Elmish
open Feliz

//////////////////////////////////////////////////////////////////////////////////////////
/// Changes in this revision:
/// - Complete section 8.1.2.
///   Note:  Routing is not complete yet.  If you click on the 'Gallery' link at the
///   top of the page you will get an error.
/// - As we already laid the groundwork for an SPA when we introduced the Folders page,
///   this is now adding to that groundwork by adding some 'global' infrastructure on the
///   generated DOM.  So, here again we are going to diverge a bit from the implementation
///   in the book.  As the book builds up to an SPA, we already have the foundation for
///   one.  So we are going to incorporate elements the book is adding into the existing
///   infrastructure.
/// - Reorganize code and make local module functions private in Gallery.fs and 
///   PhotoFolders.fs.
//////////////////////////////////////////////////////////////////////////////////////////


type Page =
    | Gallery
    | Folders
    | NotFound

type Msg =
    | GalleryMsg of Gallery.Msg
    | FolderMsg of PhotoFolders.Msg
    | SwitchPage of Page

type AppModel =
    { GalleryModel: Gallery.Model
      FolderModel: PhotoFolders.Model 
      CurrentPage: Page }


type LinkDetail = {
    Url : string
    Caption : string
}


let initialModel =
    { GalleryModel = Gallery.initialModel
      FolderModel  = PhotoFolders.initialModel
      CurrentPage  = Folders }

let init (flags: float) = 
    // let gm, gc = Gallery.init flags
    // { initialModel with GalleryModel = gm }, Cmd.map Msg.GalleryMsg gc
    let fm, fc = PhotoFolders.init ()
    { initialModel with FolderModel = fm }, Cmd.map Msg.FolderMsg fc

let update (msg : Msg) (model : AppModel) : ( AppModel * Cmd<Msg> ) =
    match msg with
    | GalleryMsg gm ->
        let newModel, cmd = Gallery.update gm model.GalleryModel
        let appCmd = Cmd.map Msg.GalleryMsg cmd
        { model with GalleryModel = newModel }, appCmd
    | FolderMsg fm ->
        let newModel, cmd = PhotoFolders.update fm model.FolderModel
        let appCmd = Cmd.map Msg.FolderMsg cmd
        { model with FolderModel = newModel }, appCmd
    | SwitchPage p -> { model with CurrentPage = p }, Cmd.none


let navLink (targetPage : Page) (details : LinkDetail) (currPage : Page) =
    let classes = if (currPage = targetPage) then ["active"] else []
    Html.li [
        prop.classes classes
        prop.children [
            Html.a [
                prop.href details.Url
                prop.text details.Caption
            ]
        ]
    ]


let viewHeader (page : Page) =
    let logo = Html.h1 [ prop.text "Photo Groove" ]
    let links = Html.ul [
        navLink Folders { Url = "/";        Caption = "Folders" } page
        navLink Gallery { Url = "/gallery"; Caption = "Gallery" } page
    ]
    Html.nav [
        logo
        links
    ]


let viewFooter =
    Html.footer [
        prop.text "One is never alone with a rubber duck. - Douglas Adams"
    ]


let viewPageContent (model : AppModel) (dispatch : Msg -> unit) =
    // Our dispatch function takes a Msg defined in the App module (this one).  However,
    // the Gallery and PhotoFolders modules return messages defined in those modules.
    // So, to dispatch those messages we need to map them back into messages in this module.
    let galleryDispatch (gm : Gallery.Msg) : unit = dispatch (Msg.GalleryMsg gm)
    let folderDispatch (fm : PhotoFolders.Msg) : unit = dispatch (Msg.FolderMsg fm)

    // With those dispatch functions defined we can now provide them to the `view` functions
    // in the page modules.
    match model.CurrentPage with
    | Page.Gallery ->
        Browser.Dom.document.title <- model.GalleryModel.Title 
        Gallery.view model.GalleryModel galleryDispatch
    | Page.Folders ->
        Browser.Dom.document.title <- model.FolderModel.Title
        PhotoFolders.view model.FolderModel folderDispatch
    | Page.NotFound -> Html.label [ prop.text "Something went wrong." ]


let view (model : AppModel) (dispatch : Msg -> unit) =
    Html.div [
        prop.children [
            viewHeader model.CurrentPage
            viewPageContent model dispatch
            viewFooter
        ]
    ]


// The following two functions map the subscriptions in the Gallery module
// to subscriptions in the App message-space
let gallerySliderSub (model : AppModel) =
    Gallery.sliderSub model.GalleryModel 
    |> Cmd.map Msg.GalleryMsg

let galleryActivitySub (model : AppModel) =
    Gallery.activitySub model.GalleryModel
    |> Cmd.map Msg.GalleryMsg

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif


Program.mkProgram init update view
|> Program.withSubscription gallerySliderSub
|> Program.withSubscription galleryActivitySub
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactBatched "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.runWith Gallery.filterLib.pastaVersion
