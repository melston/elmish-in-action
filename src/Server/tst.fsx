#I "bin/Debug/netcoreapp3.0"
#r "Thoth.Json.Net"
#r "Newtonsoft.Json"

open Thoth.Json.Net

let evt = """
{
    "detail": {
        "userSlidTo": 4,
        "sliderName": "Hue"
    }
}"""

type SlideEvtDetail =
    { UserSlidTo : int
      SliderName : string }

    static member Decoder : Decoder<SlideEvtDetail> =
        Decode.object (fun get -> {
            UserSlidTo  = get.Required.Field "userSlidTo"  Decode.int
            SliderName  = get.Required.Field "sliderName"  Decode.string
        })

type SlideEvt =
    { Detail : SlideEvtDetail }

    static member Decoder : Decoder<SlideEvt> =
        Decode.object (fun get -> {
            Detail = get.Required.Field "detail" SlideEvtDetail.Decoder
        })

//let d = Decode.at ["detail"; "userSlidTo"] Decode.int evt 
//Decode.fromString SlideEvtDetail.Decoder evt
//Decode.at ["detail"; "userSlidTo"] Decode.int evt 

// The type `Decoder<int>` is equivalent to
// `string -> JsonValue -> Result<int, DecoderError>
let id = Decode.at ["detail"; "userSlidTo"] Decode.int 
Decode.fromString id evt

Decode.fromString SlideEvt.Decoder evt
